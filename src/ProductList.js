import React from 'react';
import { Link } from 'react-router-dom';

class ProductRow extends React.Component {
  render() {
    return (
      <tr>
        <td style={this.props.product.stocked ? {color: 'black'} : {color: 'red'}}><Link to={`/products/${this.props.product.id}`}>{this.props.product.name}</Link></td>
        <td>{this.props.product.price}</td>
      </tr>
    );
  }
}

class ProductCategoryRow extends React.Component {
  render() {
    return (
      <th>
        <td>{this.props.name}</td>
      </th>
    );
  }
}

class ProductList extends React.Component {
  render() {
    let lastCategory = null;
    const productRows = [];
    const products = this.props.data;
    products.forEach((product) => {
      if (this.props.isStockedOnly && !product.stocked) {
        return;
      }
      if (this.props.filterText && !product.name.toLowerCase().includes(this.props.filterText.toLowerCase())) {
        return;
      }
      if (lastCategory == null || product.category !== lastCategory) {
        lastCategory = product.category;
        productRows.push(<ProductCategoryRow
                            name={product.category}
                            key={product.category} />);
      }
      productRows.push(<ProductRow
                  key={product.id}
                  product={product} />);
      }
    );

    return (
      <div className="ProductTable">
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {productRows}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ProductList;