import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import './App.css';
import ProductsSection from './ProductsSection.js'

const PRODUCTS = [
  {id: '0', category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
  {id: '1', category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
  {id: '2', category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
  {id: '3', category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
  {id: '4', category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
  {id: '5', category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
];

const NavBar = (
  <div>
    <ul>
      <li><Link to="/">Home</Link></li>
      <li><Link to="/products">Products</Link></li>
    </ul>
  </div>
)

const MyProducts = (props) => (
    <ProductsSection {...props} products={PRODUCTS}/>  
)

const Home = () => (
  <h1>Home</h1>
);

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <header className="App-header">
          {NavBar}
          </header>
          <div>
            <Route exact path="/" component={Home}/>
            <Route path="/products" component={MyProducts} />
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
