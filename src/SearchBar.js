import React from 'react';

class SearchBar extends React.Component {
  render() {
    return (
      <div className="SearchBar">
          <label>
            <input
              type="text"
              name="searchFilter"
              onChange={this.props.handleFilterTextValueChanged}
              value={this.props.filterText}
              placeholder="Search..." />
          </label><br/>
          <label>
            <input
              name="inStock"
              checked={this.props.isStockedOnly}
              onChange={this.props.handleStockedValueChanged}
              type="checkbox" />
            Only show products in stock
        </label>

      </div>
    );
  }
}

export default SearchBar;