import React from 'react';
import SearchBar from './SearchBar.js';
import ProductList from './ProductList.js';
import { Route } from 'react-router-dom';

class ProductsSection extends React.Component {
  constructor(props) {
    super(props);
    this.products = this.props.products;
    this.state = {
      filterText : '',
      isStockedOnly : false
    };
  }

  handleStockedValueChanged(event) {
    this.setState({
      isStockedOnly : event.target.checked
    });
  }

  handleFilterTextValueChanged(event) {
    this.setState({
      filterText : event.target.value
    });
  }

  myProductList = (props) => (
        <ProductList 
          {...props}
          data={this.products}
          isStockedOnly={this.state.isStockedOnly}
          filterText={this.state.filterText} />
  )

  productDetails = (props) => (
    <div>
      <h2>Product Details</h2>
      <span>
      Name: {this.products[props.match.params.productId].name}<br/>
      Price: {this.products[props.match.params.productId].price}<br/>
      <span style={this.products[props.match.params.productId].stocked ? {color: 'black'} : {color: 'red'}}>{this.products[props.match.params.productId].stocked ? "In Stock" : "Out Of Stock"}</span>
    </span>
    </div>
  )

  render() {
    return (
      <div>
        <SearchBar
          isStockedOnly={this.state.isStockedOnly}
          filterText={this.state.filterText}
          handleStockedValueChanged={(e) => this.handleStockedValueChanged(e)}
          handleFilterTextValueChanged={(e) => this.handleFilterTextValueChanged(e)} />
        <Route path="/" component={this.myProductList}/>
        <div>
          <Route path={`${this.props.match.url}/:productId`} component={this.productDetails}/>
        </div>
      </div>
    );
  }
}

export default ProductsSection;
